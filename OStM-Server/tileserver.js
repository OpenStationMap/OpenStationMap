var zlib = require('zlib');
var express = require('express');
var mapnik = require('mapnik');
var Promise = require('promise');
var SphericalMercator = require('sphericalmercator');
var Pbf = require('pbf');
const path = require('path');
var fs = require('fs');
var toobusy = require('toobusy-js');
var cluster = require('cluster');
var numCPUs = require('os').cpus().length;
configuration = require('./configuration.json');

toobusy.maxLag(70);
var tilesfolder = path.resolve('tiles') + "/";
var port = configuration.port;
var queries = configuration.queries;
var app = express();
mapnik.register_default_input_plugins();


if (cluster.isMaster) {
    for (var i = 0; i < numCPUs; i++) {
        cluster.fork();
    }
}
else {


app.use(function(req, res, next) {
    console.log("check if too busy");
  if (toobusy()) {
    console.log("toobusy!!!!!!!!!!!!!!!!!!!!!!!")
    res.setHeader("Access-Control-Allow-Origin", "*"); 
    res.send(503, "I'm busy right now, sorry.");
  } else {
    next();
  }
});

app.get('/vector-tiles/:z/:x/:y.pbf', function(req, res){ 
    
    var options = {
        x: parseInt(req.params.x),
        y: parseInt(req.params.y),
        z: parseInt(req.params.z)
    };
    
    if(options.z < 16 || options.z > 20){
        return res.status(403).send({message: 'Zoomlevel not available'});
    }
     
    var subfolder = req.params.x; 
    var tilename = options.z + "/" + subfolder.substring(0,1) + "/" +
                   options.x + "-" + options.y;    
    var tilepath = tilesfolder + tilename; 
    console.log("new Request");
    console.log(tilepath);

    fs.stat(tilepath, function (error, stats) {
        
        if (error) {
            console.log("tile didn't exist")
            res.setHeader("Access-Control-Allow-Origin", "*");
            res.setHeader('Content-Encoding', 'deflate');
            res.setHeader('Content-Type', 'application/x-protobuf');    
  
            makeTile(options, queries, tilepath).then(
                function(vectorTile) {
                    res.send(vectorTile); 
                }            
            );
            
        }        
        else if(stats !== undefined){
            var mtime = stats['mtime'].getFullYear();
            console.log("year: " + mtime);
            if(mtime == "1970"){
                
                console.log("tile exist but was touched"); 
                res.setHeader("Access-Control-Allow-Origin", "*");
                res.setHeader('Content-Encoding', 'deflate');
                res.setHeader('Content-Type', 'application/x-protobuf');    
  
                makeTile(options, queries, tilepath).then(
                        function(vectorTile) {
                            res.send(vectorTile); 
                        }            
                );
                

            }
            else{
                fs.readFile(tilepath, function read(err,data){
                if (err) return res.status(500).send(err.message);      
                res.setHeader("Access-Control-Allow-Origin", "*");
                res.setHeader('Content-Encoding', 'deflate');
                res.setHeader('Content-Type', 'application/x-protobuf');
                console.log("delivered old tile: " + tilepath);
                res.send(data);
                });
            }  
        }
        
            
    }); 
});

app.get('/vector-tiles/:z/:x/:y.pbf/dirty', function(req, res){    
     
     var options = {x: parseInt(req.params.x),y: parseInt(req.params.y),z: parseInt(req.params.z)};  
     var subfolder = req.params.x; 
     var tilename = options.z + "/" + subfolder.substring(0,1) + "/" +
                    options.x + "-" + options.y;    
     var tilepath = tilesfolder + tilename; 

     makeTile(options, queries, tilepath).then(
                function(vectorTile) {
		    res.setHeader("Access-Control-Allow-Origin", "*");
                    res.type('text/plain');    
                    res.send("dirty request received. Created new tile.");
		    
                }            
     ); 	
});


function makeTile(options, queries, tilepath){

    var mercator = new SphericalMercator({size: 256});
    var extent = mercator.bbox(options.x, options.y, options.z, false);
	
    var map = new mapnik.Map(256, 256); 
    map.extent = extent;

    for (var layerName in queries) {
        var query = queries[layerName];
        var layer = new mapnik.Layer(layerName);

        layer.datasource = new mapnik.Datasource({
            type: 'postgis',
            dbname: configuration.database,
            table: query, 
            user: configuration.username,
            password: configuration.password,       
            geometry_field: configuration.geometry_field
        });        
        map.add_layer(layer);    
    }
    
    var tile = new Promise(function (resolve, reject) {
           
        var vtile = new mapnik.VectorTile(parseInt(options.z), parseInt(options.x), parseInt(options.y)); 
     
       map.render(vtile, function (err, vtile) {
            if (err) return reject(err);
	    
            zlib.deflate(vtile.getData(), function(err, data) {  
                    if (err) return reject(err);		                   
                    fs.writeFile(tilepath, data, function(err) {
                                if(err) {return console.log("could not save file" + err);}
				
                                console.log("saved new tile: " + tilepath);
                                });                
                                              
                                          
                    resolve(data);

            });	
        });
    });  
    return tile;
}

app.listen(port, '127.0.0.1');
console.log("listening on port: " + port);
}

