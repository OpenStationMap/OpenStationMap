#!/usr/bin/env python.

import subprocess
import sys
import os
import math


def createparent(tile, minzoom):
	tileparents = []
	tempoX = tile['x']
	tempoY = tile['y']
	tempoZoom = tile['zoom']	
	
	while tempoZoom >= minzoom:	
		
		tileparent = {'zoom': tempoZoom, 'x': tempoX, 'y': tempoY}
		tileparents.append(tileparent)		
		tempoZoom -= 1		
		tempoX /= 2	
		tempoY /= 2	

	return tileparents

minzoom = 17
subpath = "/home/gisuser/openstationmap/tiles/"
tilelist = []
inputtile = str(sys.argv[1]).split('/')
tile = {'zoom': int(inputtile[0]), 'x': int(inputtile[1]), 'y': int(inputtile[2])}

# create parent tiles includive start tile
tilelist = createparent(tile, minzoom)
print "+++ Start +++"

for tile in tilelist:

	zoom = str(tile['zoom'])
	subfolder = str(tile['x'])[:1]
	tilex = str(tile['x'])
	tiley = str(tile['y'])
	tilenumber = str(tile)
	path = subpath + zoom + "/" + subfolder + "/" + tilex + "-" + tiley

	if os.path.exists(path): 		
		atime = os.stat(path).st_atime
		os.utime(path,(atime,0))
		print "tile timestamp changed: " + path

	else:
		print "nothing to touch: " + path				
