#!/bin/bash

path='/home/gisuser/openstationmap/database'
cd $path

lockdir=updateosmdb.lock

if mkdir "$lockdir"
then    # directory did not exist, but was created successfully
    	echo >&2 "set lock successfully"
	echo "+++++++ Start ++++++++"   	
	echo UTC-TIME: `date --utc` 
	timestamp=$(<timestamp.txt)
	echo "old dtimestamp:" "$timestamp"
	timestamp_temp=$(date --utc +%Y'-'%m'-'%d'T'%H':'%M':'%S'Z') 
	echo "osmupdate"
	osmupdate planet-latest-filtered.o5m "$timestamp" planet-latest-new.o5m  --verbose 

	if [ ! -f planet-latest-new.o5m ]; then
    		echo "File planet-latest-new.o5m does not exist. No file to upload"    	
		echo "delete lockfile"
		trap 'rm -rf "$lockdir"' 0   
		echo "+++++++ ENDE ++++++++"
		exit 1
	fi
	echo "$timestamp_temp" > timestamp.txt
	echo "---------"
	echo "osmfilter"
	osmfilter planet-latest-new.o5m --keep=level= --drop-author --out-o5m >planet-latest-filtered-new.o5m
	echo "---------"
	echo "delete new unfiltered updatefile"
	rm planet-latest-new.o5m
	echo "---------"
	echo "osmconvert"
	osmconvert planet-latest-filtered.o5m planet-latest-filtered-new.o5m --drop-author --diff-contents --fake-lonlat >datachange.osc
	echo "---------"
	echo "delete old filtered updatefile"
	rm planet-latest-filtered.o5m
	echo "---------"
	echo "change filename"
	mv planet-latest-filtered-new.o5m planet-latest-filtered.o5m
	echo "---------"
	echo "start update db"
	osm2pgsql --style /home/gisuser/openstationmap/database/level.style  --append -d planet -U gisuser --slim --hstore --hstore-match-only --expire-tiles 20 --expire-output expired_tiles datachange.osc
	echo "---------"
	echo "delete changefile"
	rm datachange.osc  	
	echo "---------"
	echo "touch tiles"
	while read -r line
	do	   	
	#whole string
	echo "tile: - $line"
	
	python touchtiles.py $line >>tilestouched.log
    	
	done < expired_tiles
	echo "---------"
	echo "delete expired_tiles list"
	rm expired_tiles
	echo "---------"
	echo "delete lockfile"
	trap 'rm -rf "$lockdir"' 0   
	echo "+++++++ ENDE ++++++++"

else
    echo >&2 "other process is running. Stop"
    exit 0
fi
