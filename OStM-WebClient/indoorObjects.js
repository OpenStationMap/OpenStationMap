
var startlevel = ["in", "level", "0;1", "0", "-1;0","-1;0;1", "-0.5;0", "0;0.5","0.5","0.5;1", "0;2"];

var indoorObjects = [];

var stairs = {    "id": "stairs",                     
                  "filter": ["all", ["==", "highway", "steps"],["!has", "width"]] 
                    
}
indoorObjects.push(stairs);

var stairs_width = {    "id": "stairs_width",                     
                        "filter": ["all", ["==", "highway", "steps"],["has", "width"]] 
                    
}
indoorObjects.push(stairs_width);

var room = {    "id": "room",                                     
                "filter": ["any", ["==", "indoor", "room"], ["==", "room", "yes"], ["has", "shop"]]                    
} 
indoorObjects.push(room);

var room_access = {    "id": "room_access",                                     
                       "filter": ["all", ["==", "indoor", "room"],["any", ["==", "access", "private"],["==", "access", "no"]]]                    
} 
indoorObjects.push(room_access);
/*
var room_outline = {    "id": "room_outline",                                     
                        "filter": ["any", ["==", "indoor", "room"], ["==", "room", "yes"]] 
                   
}
indoorObjects.push(room_outline);
*/
var corridor = {    "id": "corridor",                     
                    "filter": ["any", ["==", "room", "corridor"],["==", "indoor", "corridor"],["==", "indoor", "area"],["==", "building_part", "floor"]] 
                   
}
indoorObjects.push(corridor);

var platform = {    "id": "platform",                     
                    "filter": ["any", ["==", "public_transport", "platform"],["==", "railway", "platform"]] 
                   
}
indoorObjects.push(platform);

var flightofstairs = {    "id": "stairwell",                     
                          "filter": ["any", ["==", "stairwell", "flight_of_stairs"], ["==", "stairwell", "yes"]] 
                   
}
indoorObjects.push(flightofstairs);

var stairlanding = {    "id": "stailanding",                     
                        "filter": ["any", ["==", "stairwell", "stair_landing"]] 
                   
}
indoorObjects.push(stairlanding);

var footway = {    "id": "footway",                     
                   "filter": ["any", ["==", "highway", "footway"]] 
                    
}
indoorObjects.push(footway);

var elevatorpoint = {    "id": "elevatorpoint",                     
                         "filter": ["any", ["==", "highway", "elevator"]] 
                    
} 
indoorObjects.push(elevatorpoint);

var service = {    "id": "service",                     
                   "filter": ["any", ["==", "public_transport", "service_center"], ["==", "public_transport", "service_point"], ["==", "shop", "ticket"], ["==", "amenity", "luggage_locker"]] 
                    
}
indoorObjects.push(service);


var service_symbol = {    "id": "service_symbol",                     
                          "filter": ["any", ["==", "public_transport", "service_center"], ["==", "public_transport", "service_point"], ["==", "shop", "ticket"]] 
                    
}
indoorObjects.push(service_symbol);

var public_transport_tickets = {    "id": "public_transport_tickets",                     
                                    "filter": ["all", ["==", "amenity", "vending_machine"],["==", "vending", "public_transport_tickets"]] 
                    
}
indoorObjects.push(public_transport_tickets);

var facilities = {    "id": "facilities",                     
                      "filter": ["any", ["==", "amenity", "toilets"], ["==", "amenity", "police"]] 
                    
}
indoorObjects.push(facilities);

var bench = {    "id": "bench",                     
                 "filter": ["all", ["==", "amenity", "bench"]] 
                    
}
indoorObjects.push(bench);

var ticket_validator = {    "id": "ticket_validator",                     
                            "filter": ["all", ["==", "vending", "ticket_validator"]] 
                    
}
indoorObjects.push(ticket_validator);

var local_ref1 = {    "id": "local_ref1",                     
                      "filter": ["all", ["==", "public_transport", "stop_position"],["==", "local_ref", "1"]] 
                    
}
indoorObjects.push(local_ref1);

var local_ref2 = {    "id": "local_ref2",                     
                      "filter": ["all", ["==", "public_transport", "stop_position"],["==", "local_ref", "2"]] 
                    
}
indoorObjects.push(local_ref2);

var local_ref3 = {    "id": "local_ref3",                     
                      "filter": ["all", ["==", "public_transport", "stop_position"],["==", "local_ref", "3"]] 
                    
}
indoorObjects.push(local_ref3);

var local_ref4 = {    "id": "local_ref4",                     
                      "filter": ["all", ["==", "public_transport", "stop_position"],["==", "local_ref", "4"]] 
                    
}
indoorObjects.push(local_ref4);

var local_ref5 = {    "id": "local_ref5",                     
                      "filter": ["all", ["==", "public_transport", "stop_position"],["==", "local_ref", "5"]]                     
}
indoorObjects.push(local_ref5);

var local_ref6 = {    "id": "local_ref6",                     
                      "filter": ["all", ["==", "public_transport", "stop_position"],["==", "local_ref", "6"]] 
                    
}
indoorObjects.push(local_ref6);

var local_ref7 = {    "id": "local_ref7",                     
                      "filter": ["all", ["==", "public_transport", "stop_position"],["==", "local_ref", "7"]] 
                    
}
indoorObjects.push(local_ref7);

var local_ref8 = {    "id": "local_ref8",                     
                      "filter": ["all", ["==", "public_transport", "stop_position"],["==", "local_ref", "8"]] 
                    
}
indoorObjects.push(local_ref8);

var local_ref9 = {    "id": "local_ref9",                     
                      "filter": ["all", ["==", "public_transport", "stop_position"],["==", "local_ref", "9"]] 
                    
}
indoorObjects.push(local_ref9);

var local_ref10 = {    "id": "local_ref10",                     
                       "filter": ["all", ["==", "public_transport", "stop_position"],["==", "local_ref", "10"]] 
                    
}
indoorObjects.push(local_ref10);

var atm = {    "id": "atm",                     
               "filter": ["all", ["==", "amenity", "atm"]] 
                    
}
indoorObjects.push(atm);

var elevator = {    "id": "elevator",                     
                    "filter": ["any", ["==", "highway", "elevator"],["==", "stairwell", "elevator"],["==", "building_part", "elevator"]] 
                    
}
indoorObjects.push(elevator);

var fence = {    "id": "fence",                     
                 "filter": ["any", ["==", "barrier", "fence"], ["==", "barrier", "railing"], ["==", "barrier", "wall"]] 
                    
}
indoorObjects.push(fence);

var door = {    "id": "door",                     
                "filter": ["all", ["has", "door"]] 
                    
}
indoorObjects.push(door);

var label_toilet = {    "id": "label_toilet",                     
                        "filter": ["any", ["==", "amenity", "toilets"]] 
                    
}
indoorObjects.push(label_toilet);

var label_luggage_locker = {    "id": "label_luggage_locker",                     
                                "filter": ["any", ["==", "amenity", "luggage_locker"]] 
                    
}
indoorObjects.push(label_luggage_locker);

var label_fast_food = {    "id": "label_fast_food",                     
                           "filter": ["any", ["==", "amenity", "fast_food"]] 
                    
}
indoorObjects.push(label_fast_food);
var label_restaurant = {    "id": "label_restaurant",                     
                            "filter": ["any", ["==", "amenity", "restaurant"]] 
                    
}
indoorObjects.push(label_restaurant);

var label_bank = {    "id": "label_bank",                     
                      "filter": ["any", ["==", "amenity", "bank"]] 
                    
}
indoorObjects.push(label_bank);

var label_bakery = {    "id": "label_bakery",                     
                        "filter": ["any", ["==", "amenity", "bakery"], ["==", "shop", "bakery"]] 
                    
}
indoorObjects.push(label_bakery);

var label_pharmacy = {    "id": "label_pharmacy",                     
                        "filter": ["all", ["==", "amenity", "pharmacy"]] 
                    
}
indoorObjects.push(label_pharmacy);

var label_florist = {    "id": "label_florist",                     
                        "filter": ["all", ["==", "shop", "florist"]] 
                    
}
indoorObjects.push(label_florist);

var label_clothes = {    "id": "label_clothes",                     
                     "filter": ["all", ["==", "shop", "clothes"]] 
                    
}
indoorObjects.push(label_clothes);

var label_car_rental = {    "id": "label_car_rental",                     
                            "filter": ["all", ["==", "amenity", "car_rental"]] 
                    
}
indoorObjects.push(label_car_rental);

var label_books = {    "id": "label_books",                     
                        "filter": ["any", ["==", "shop", "books"], ["==", "shop", "newsagent"]] 
                    
}
indoorObjects.push(label_books);

var rail = {    "id": "rail",                     
                "filter": ["any", ["==", "railway", "rail"], ["==", "railway", "light_rail"]] 
                    
}
indoorObjects.push(rail);

var label_cafe = {    "id": "label_cafe",                     
                      "filter": ["any", ["==", "amenity", "cafe"], ["==", "shop", "coffee"]] 
                    
}
indoorObjects.push(label_cafe);

var label_information = {    "id": "label_information",                     
                      "filter": ["any", ["==", "tourism", "information"]] 
                    
}
indoorObjects.push(label_information);

var text_shop = {    "id": "text_shop",                     
                     "filter": ["all", ["==", "room", "shop"], ["has", "name"]] 
                    
}
indoorObjects.push(text_shop);

var building_trainstation_level = {    "id": "building_trainstation_level",                     
                                       "filter": ["all", ["==", "building", "train_station"]] 
                    
}
indoorObjects.push(building_trainstation_level);

var conveying_up = {    "id": "conveying_up",                     
                        "filter": ["any", ["all", ["==", "highway", "steps"], ["==", "conveying", "forward"], ["==", "incline", "up"]], ["all", ["==", "highway", "steps"], ["==", "conveying", "backward"], ["==", "incline", "down"]]] 
                    
}
indoorObjects.push(conveying_up);

var conveying_down = {    "id": "conveying_down",                     
                        "filter": ["any",["all", ["==", "highway", "steps"],["==", "conveying", "forward"], ["==", "incline", "down"]], ["all", ["==", "highway", "steps"], ["==", "conveying", "backward"], ["==", "incline", "up"]]]
                    
}
indoorObjects.push(conveying_down);


