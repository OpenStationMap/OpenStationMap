var style = {
    "version": 8,
    //"name": "Basic",
    "metadata": {
        "mapbox:autocomposite": true
    },   
    "sources": {
        "mapbox": {
            "url": "mapbox://mapbox.mapbox-streets-v7",
            "type": "vector"            
        },  
         "indoor_source": {
            "type": "vector",
            "tiles": ["http://openstationmap.org/vector-tiles/{z}/{x}/{y}.pbf"], // ["http://[::ffff:55d6:7e5f]:1234/vector-tiles/{z}/{x}/{y}.pbf"],
            "minzoom":16

        },  // http://localhost:1234/vector-tiles/'$line'/dirty'
        
        "mapzen": {
            "type": "vector",
            "tiles": ["https://tile.mapzen.com/mapzen/vector/v1/all/{z}/{x}/{y}.mvt?api_key=vector-tiles-LM25tq4"],
          //  http://tile.mapzen.com/mapzen/vector/v1/all/{z}/{x}/{y}.mvt?api_key=mapzen-xxxxxxx
            "minzoom":1,
            "maxzoom":18

        }, 
   // "https://vector.mapzen.com/osm/all/{z}/{x}/{y}.mvt?api_key=vector-tiles-LM25tq4"         

    },
    //"sprite": "http://85.214.126.95/settgast/hompageTest/sprite/symbol",
    "sprite": "http://85.214.126.95/vectormap/0.1.0/sprite/symbol",
    //"mapbox://sprites/mapbox/basic-v8",
    "glyphs": "mapbox://fonts/mapbox/{fontstack}/{range}.pbf",
    "layers": [
        {
            "id": "background",
            "type": "background",
            "paint": {
                "background-color": "#dedede"
            },
            "interactive": true
        },
        {
            "id": "landuse_overlay_national_park",
            "maxzoom":18,
            "type": "fill",
            "source": "mapbox",
            "source-layer": "landuse_overlay",
            "filter": [
                "==",
                "class",
                "national_park"
            ],
            "paint": {
                "fill-color": "#d2edae",
                "fill-opacity": 0.75
            },
            "interactive": true
        },
        {
            "id": "landuse_park",
            "type": "fill",
            "source": "mapbox",
            "source-layer": "landuse",
            "maxzoom":18,
            "filter": [
                "==",
                "class",
                "park"
            ],
            "paint": {
                "fill-color": "#d2edae"
            },
            "interactive": true
        },
        {
            "id": "waterway",
            "type": "line",
            "source": "mapbox",
            "source-layer": "waterway",
            "maxzoom":18,
            "filter": [
                "all",
                [
                    "==",
                    "$type",
                    "LineString"
                ],
                [
                    "in",
                    "class",
                    "river",
                    "canal"
                ]
            ],
            "paint": {
                "line-color": "#a0cfdf",
                "line-width": {
                    "base": 1.4,
                    "stops": [
                        [
                            8,
                            0.5
                        ],
                        [
                            20,
                            15
                        ]
                    ]
                }
            },
            "interactive": true
        },
        {
            "id": "water",
            "type": "fill",
            "source": "mapbox",
            "source-layer": "water",
            "maxzoom":18,
            "paint": {
                "fill-color": "#a0cfdf"
            },
            "interactive": true
        },        
        {
            "id": "building",
            "type": "fill",
            "source": "mapbox",
            "source-layer": "building",
            "paint": {
                "fill-color": "#d6d6d6" // #d6d6d6 #D9CCBE
            },
            "interactive": true
        },        
        {
            "interactive": true,
            "layout": {
                "line-cap": "butt",
                "line-join": "miter"
            },
            "filter": [
                "all",
                [
                    "==",
                    "$type",
                    "LineString"
                ],
                [
                    "all",
                    [
                        "in",
                        "class",
                        "motorway_link",
                        "street",
                        "street_limited",
                        "service",
                        "track",
                        "pedestrian",
                        "path",
                        "link"
                    ],
                    [
                        "==",
                        "structure",
                        "tunnel"
                    ]
                ]
            ],
            "type": "line",
            "source": "mapbox",
            "id": "tunnel_minor",
            "maxzoom":18,
            "paint": {
                "line-color": "#efefef",
                "line-width": {
                    "base": 1.55,
                    "stops": [
                        [
                            4,
                            0.25
                        ],
                        [
                            20,
                            30
                        ]
                    ]
                },
                "line-dasharray": [
                    0.36,
                    0.18
                ]
            },
            "source-layer": "road"
        },
        {
            "interactive": true,
            "layout": {
                "line-cap": "butt",
                "line-join": "miter"
            },
            "filter": [
                "all",
                [
                    "==",
                    "$type",
                    "LineString"
                ],
                [
                    "all",
                    [
                        "in",
                        "class",
                        "motorway",
                        "primary",
                        "secondary",
                        "tertiary",
                        "trunk"
                    ],
                    [
                        "==",
                        "structure",
                        "tunnel"
                    ]
                ]
            ],
            "type": "line",
            "source": "mapbox",
            "id": "tunnel_major",
            "maxzoom":18,
            "paint": {
                "line-color": "#fff",
                "line-width": {
                    "base": 1.4,
                    "stops": [
                        [
                            6,
                            0.5
                        ],
                        [
                            20,
                            30
                        ]
                    ]
                },
                "line-dasharray": [
                    0.28,
                    0.14
                ]
            },
            "source-layer": "road"
        },
        {
            "interactive": true,
            "layout": {
                "line-cap": "round",
                "line-join": "round"
            },
            "filter": [
                "all",
                [
                    "==",
                    "$type",
                    "LineString"
                ],
                [
                    "all",
                    [
                        "in",
                        "class",
                        "motorway_link",
                        "street",
                        "street_limited",
                        "service",
                        "track",
                        "pedestrian",
                       // "path",  // tracks inside building
                        "link"
                    ],
                    [
                        "in",
                        "structure",
                        "none",
                        "ford"
                    ]
                ]
            ],
            "type": "line",
            "source": "mapbox",
            "id": "road_minor",
            "maxzoom":18,
            "paint": {
                "line-color": "#efefef",
                "line-width": {
                    "base": 1.55,
                    "stops": [
                        [
                            4,
                            0.25
                        ],
                        [
                            20,
                            30
                        ]
                    ]
                }
            },
            "source-layer": "road"
        },
        {
            "interactive": true,
            "layout": {
                "line-cap": "round",
                "line-join": "round"
            },
            "filter": [
                "all",
                [
                    "==",
                    "$type",
                    "LineString"
                ],
                [
                    "all",
                    [
                        "in",
                        "class",
                        "motorway",
                        "primary",
                        "secondary",
                        "tertiary",
                        "trunk"
                    ],
                    [
                        "in",
                        "structure",
                        "none",
                        "ford"
                    ]
                ]
            ],
            "type": "line",
            "source": "mapbox",
            "id": "road_major",
            "maxzoom":18,
            "paint": {
                "line-color": "#fff",
                "line-width": {
                    "base": 1.4,
                    "stops": [
                        [
                            6,
                            0.5
                        ],
                        [
                            20,
                            30
                        ]
                    ]
                }
            },
            "source-layer": "road"
        },
        {
            "interactive": true,
            "layout": {
                "line-cap": "butt",
                "line-join": "miter"
            },
            "filter": [
                "all",
                [
                    "==",
                    "$type",
                    "LineString"
                ],
                [
                    "all",
                    [
                        "in",
                        "class",
                        "motorway_link",
                        "street",
                        "street_limited",
                        "service",
                        "track",
                        "pedestrian",
                        "path",
                        "link"
                    ],
                    [
                        "==",
                        "structure",
                        "bridge"
                    ]
                ]
            ],
            "type": "line",
            "source": "mapbox",
            "id": "bridge_minor case",
            "maxzoom":18,
            "paint": {
                "line-color": "#dedede",
                "line-width": {
                    "base": 1.6,
                    "stops": [
                        [
                            12,
                            0.5
                        ],
                        [
                            20,
                            10
                        ]
                    ]
                },
                "line-gap-width": {
                    "base": 1.55,
                    "stops": [
                        [
                            4,
                            0.25
                        ],
                        [
                            20,
                            30
                        ]
                    ]
                }
            },
            "source-layer": "road"
        },
        {
            "interactive": true,
            "layout": {
                "line-cap": "butt",
                "line-join": "miter"
            },
            "filter": [
                "all",
                [
                    "==",
                    "$type",
                    "LineString"
                ],
                [
                    "all",
                    [
                        "in",
                        "class",
                        "motorway",
                        "primary",
                        "secondary",
                        "tertiary",
                        "trunk"
                    ],
                    [
                        "==",
                        "structure",
                        "bridge"
                    ]
                ]
            ],
            "type": "line",
            "source": "mapbox",
            "id": "bridge_major case",
            "maxzoom":18,
            "paint": {
                "line-color": "#dedede",
                "line-width": {
                    "base": 1.6,
                    "stops": [
                        [
                            12,
                            0.5
                        ],
                        [
                            20,
                            10
                        ]
                    ]
                },
                "line-gap-width": {
                    "base": 1.55,
                    "stops": [
                        [
                            4,
                            0.25
                        ],
                        [
                            20,
                            30
                        ]
                    ]
                }
            },
            "source-layer": "road"
        },
        {
            "interactive": true,
            "layout": {
                "line-cap": "round",
                "line-join": "round"
            },
            "filter": [
                "all",
                [
                    "==",
                    "$type",
                    "LineString"
                ],
                [
                    "all",
                    [
                        "in",
                        "class",
                        "motorway_link",
                        "street",
                        "street_limited",
                        "service",
                        "track",
                        "pedestrian",
                        "path",
                        "link"
                    ],
                    [
                        "==",
                        "structure",
                        "bridge"
                    ]
                ]
            ],
            "type": "line",
            "source": "mapbox",
            "id": "bridge_minor",
            "maxzoom":18,
            "paint": {
                "line-color": "#efefef",
                "line-width": {
                    "base": 1.55,
                    "stops": [
                        [
                            4,
                            0.25
                        ],
                        [
                            20,
                            30
                        ]
                    ]
                }
            },
            "source-layer": "road"
        },
        {
            "interactive": true,
            "layout": {
                "line-cap": "round",
                "line-join": "round"
            },
            "filter": [
                "all",
                [
                    "==",
                    "$type",
                    "LineString"
                ],
                [
                    "all",
                    [
                        "in",
                        "class",
                        "motorway",
                        "primary",
                        "secondary",
                        "tertiary",
                        "trunk"
                    ],
                    [
                        "==",
                        "structure",
                        "bridge"
                    ]
                ]
            ],
            "type": "line",
            "source": "mapbox",
            "id": "bridge_major",
            "maxzoom":18,
            "paint": {
                "line-color": "#fff",
                "line-width": {
                    "base": 1.4,
                    "stops": [
                        [
                            6,
                            0.5
                        ],
                        [
                            20,
                            30
                        ]
                    ]
                }
            },
            "source-layer": "road"
        },
        {
            "interactive": true,
            "layout": {
                "line-cap": "round",
                "line-join": "round"
            },
            "filter": [
                "all",
                [
                    "==",
                    "$type",
                    "LineString"
                ],
                [
                    "all",
                    [
                        "<=",
                        "admin_level",
                        2
                    ],
                    [
                        "==",
                        "maritime",
                        0
                    ]
                ]
            ],
            "type": "line",
            "source": "mapbox",
            "id": "admin_country",
            "paint": {
                "line-color": "#8b8a8a",
                "line-width": {
                    "base": 1.3,
                    "stops": [
                        [
                            3,
                            0.5
                        ],
                        [
                            22,
                            15
                        ]
                    ]
                }
            },
            "source-layer": "admin"
        },
        {
            "interactive": true,
            "minzoom": 5,
            "layout": {
                "icon-image": "{maki}-11",
                "text-offset": [
                    0,
                    0.5
                ],
                "text-field": "{name_en}",
                "text-font": [
                    "Open Sans Semibold",
                    "Arial Unicode MS Bold"
                ],
                "text-max-width": 8,
                "text-anchor": "top",
                "text-size": 11,
                "icon-size": 1
            },
            "filter": [
                "all",
                [
                    "==",
                    "$type",
                    "Point"
                ],
                [
                    "all",
                    [
                        "==",
                        "scalerank",
                        1
                    ],
                    [
                        "==",
                        "localrank",
                        1
                    ]
                ]
            ],
            "type": "symbol",
            "source": "mapbox",
            "id": "poi_label",
            "paint": {
                "text-color": "#666",
                "text-halo-width": 1,
                "text-halo-color": "rgba(255,255,255,0.75)",
                "text-halo-blur": 1
            },
            "source-layer": "poi_label"
        },
        {
            "interactive": true,
            "layout": {
                "symbol-placement": "line",
                "text-field": "{name_en}",
                "text-font": [
                    "Open Sans Semibold",
                    "Arial Unicode MS Bold"
                ],
                "text-transform": "uppercase",
                "text-letter-spacing": 0.1,
                "text-size": {
                    "base": 1.4,
                    "stops": [
                        [
                            10,
                            8
                        ],
                        [
                            20,
                            14
                        ]
                    ]
                }
            },
            "filter": [
                "all",
                [
                    "==",
                    "$type",
                    "LineString"
                ],
                [
                    "in",
                    "class",
                    "motorway",
                    "primary",
                    "secondary",
                    "tertiary",
                    "trunk"
                ]
            ],
            "type": "symbol",
            "source": "mapbox",
            "id": "road_major_label",
            "maxzoom":18,
            "paint": {
                "text-color": "#666",
                "text-halo-color": "rgba(255,255,255,0.75)",
                "text-halo-width": 2
            },
            "source-layer": "road_label"
        },
        {
            "interactive": true,
            "minzoom": 8,
            "layout": {
                "text-field": "{name_en}",
                "text-font": [
                    "Open Sans Semibold",
                    "Arial Unicode MS Bold"
                ],
                "text-max-width": 6,
                "text-size": {
                    "stops": [
                        [
                            6,
                            12
                        ],
                        [
                            12,
                            16
                        ]
                    ]
                }
            },
            "filter": [
                "all",
                [
                    "==",
                    "$type",
                    "Point"
                ],
                [
                    "in",
                    "type",
                    "town",
                    "village",
                    "hamlet",
                    "suburb",
                    "neighbourhood",
                    "island"
                ]
            ],
            "type": "symbol",
            "source": "mapbox",
            "id": "place_label_other",
            "maxzoom":18,
            "paint": {
                "text-color": "#666",
                "text-halo-color": "rgba(255,255,255,0.75)",
                "text-halo-width": 1,
                "text-halo-blur": 1
            },
            "source-layer": "place_label"
        },
        {
            "interactive": true,
            "layout": {
                "text-field": "{name_en}",
                "text-font": [
                    "Open Sans Bold",
                    "Arial Unicode MS Bold"
                ],
                "text-max-width": 10,
                "text-size": {
                    "stops": [
                        [
                            3,
                            12
                        ],
                        [
                            8,
                            16
                        ]
                    ]
                }
            },
            "maxzoom": 16,
            "filter": [
                "all",
                [
                    "==",
                    "$type",
                    "Point"
                ],
                [
                    "==",
                    "type",
                    "city"
                ]
            ],
            "type": "symbol",
            "source": "mapbox",
            "id": "place_label_city",
            "paint": {
                "text-color": "#666",
                "text-halo-color": "rgba(255,255,255,0.75)",
                "text-halo-width": 1,
                "text-halo-blur": 1
            },
            "source-layer": "place_label"
        },
        {
            "interactive": true,
            "layout": {
                "text-field": "{name_en}",
                "text-font": [
                    "Open Sans Regular",
                    "Arial Unicode MS Regular"
                ],
                "text-max-width": 10,
                "text-size": {
                    "stops": [
                        [
                            3,
                            14
                        ],
                        [
                            8,
                            22
                        ]
                    ]
                }
            },
            "maxzoom": 12,
            "filter": [
                "==",
                "$type",
                "Point"
            ],
            "type": "symbol",
            "source": "mapbox",
            "id": "country_label",
            "paint": {
                "text-color": "#666",
                "text-halo-color": "rgba(255,255,255,0.75)",
                "text-halo-width": 1,
                "text-halo-blur": 1
            },
            "source-layer": "country_label"
        },
        {        
            "id": "building_station",            
            "type": "fill",
            "source": "mapzen",
            "source-layer": "buildings",
            "filter": ["all",["==", "$type", "Polygon"],["==", "kind", "train_station"]],
            "minzoom": 14,
            "paint": {
                "fill-color": "#e5e5e5" //,   // #D5D5D5
                //"fill-outline-color": "black",

                
            } 
        },
         {        
            "id": building_trainstation_level.id,
            "type": "fill",
            "source": "indoor_source",    
            "source-layer": "polygon",
            "filter": 
                    ["all", startlevel,
                    building_trainstation_level.filter                    
                    ],    
            "paint": {
            "fill-color": "#D5D5D5",
            "fill-outline-color": "black"
            }
        },  
        {  //--------------- major railway
            "interactive": true,    
            "filter": [
                "all",
                [
                    "==",
                    "$type",
                    "LineString"
                ],
                [
                    "all",
                    [
                        "in",
                        "class",
                        "major_rail",
                       
                    ]                   
                ]
            ],
            "type": "line",
            "source": "mapbox",
            "id": "railway_major",
            "maxzoom": 16, 
            "paint": {
                "line-color": "#4c4c4c",
                "line-width": {
                    "base": 1.55,
                    "stops": [
                        [
                            4,
                            0.25
                        ],
                        [
                            20,
                            15
                        ]
                    ]
                },
                'line-dasharray': [2.5,0.5]
            },
            "source-layer": "road"
        },
        {    //--------------- minor railway
            "interactive": true,          
            "filter": [
                "all",
                [
                    "==",
                    "$type",
                    "LineString"
                ],
                [
                    "all",
                    [
                        "in",
                        "class",
                        "minor_rail",
                       
                    ]                   
                ]
            ],
            "type": "line",
            "source": "mapbox",
            "id": "railway_minor",
            "maxzoom": 16, 
            "paint": {
                "line-color": "#4c4c4c",
                "line-width": {
                    "base": 1.55,
                    "stops": [
                        [
                            4,
                            0.25
                        ],
                        [
                            20,
                            15
                        ]
                    ]
                },
                'line-dasharray': [2.5,0.5]
            },
            "source-layer": "road"
        },        
        //+++++++++ Start indoor ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        {
            "id": rail.id,           
            "type": "line",
            "source": "indoor_source",    
            "source-layer": "line",
            "filter": ["all", startlevel, 
                      rail.filter
                      ],   
            "paint": {
                "line-color": "#4c4c4c",
                "line-width": {
                    "base": 1.55,
                    "stops": [
                        [
                            4,
                            0.25
                        ],
                        [
                            20,
                            15
                        ]
                    ]
                },
                'line-dasharray': [2.5,0.5]
            },
        },  
       
        /*
        {
            "id": room_outline.id,
            "type": "fill",
            "source": "indoor_source",    
            "source-layer": "polygon",
            "filter":
                     ["all", startlevel,
                     ["any", room_outline.filter]                 
                     ],     
            "paint": {
                "fill-color": "rgba(0,0,0,0)" ,
                "fill-outline-color": "#7E7760"//#bfbfbf",
            }
        },  
        */
           {        
            "id": corridor.id,
            "type": "fill",
            "source": "indoor_source",    
            "source-layer": "polygon",
            "filter": 
                    ["all", startlevel,
                    corridor.filter                    
                    ],    
            "paint": {
            "fill-color": "#EDEDED"//,
            //"fill-outline-color": "#bfbfbf",
            }
        },  
        {
            "id": room.id,
            "type": "fill",
            "source": "indoor_source",    
            "source-layer": "polygon",
            "filter":
                     ["all", startlevel,
                     ["any", room.filter]                 
                     ],     
            "paint": {
                "fill-color": "#FCC21F" ,
                "fill-outline-color": "#7E7760"  //#bfbfbf",
            }
        },
        {
            "id": room_access.id,
            "type": "fill",
            "source": "indoor_source",    
            "source-layer": "polygon",
            "filter":
                     ["all", startlevel,
                     ["any", room_access.filter]                 
                     ],     
            "paint": {
                "fill-color": "#D5D5D5" ,
                "fill-outline-color": "#7E7760"  //#bfbfbf",
            }
        },        
        {
            "id": flightofstairs.id,
            "type": "fill",
            "source": "indoor_source",    
            "source-layer": "polygon",
            "filter": ["all",startlevel, 
                      flightofstairs.filter
                      ],   
            "paint": {
                "fill-color": "#C4C8CC", 
                "fill-outline-color": "#7e7f80",
            }
        },
        {
            "id": stairlanding.id,
            "type": "fill",
            "source": "indoor_source",    
            "source-layer": "polygon",
            "filter": ["all", startlevel, 
                      stairlanding.filter
                      ],   
            "paint": {
                "fill-color": "#B2B2B2", 
                "fill-outline-color": "#7e7f80",
            }
        }, 
         {
              "id": stairs.id,
            "type": "line",
            "source": "indoor_source",    
            "source-layer": "line",
            "minzoom": 17,
            "filter": ["all", startlevel, 
                      stairs.filter
                      ],   
            "paint": {
                'line-color': '#7e7f80',
                'line-width': {                    
                     "stops": [[18,9], [19,18], [20,32]]
                },                
                'line-dasharray': [0.05,0.1] // 0.2,0.4 first number line second number gap
                }
        },  
        {
              "id": stairs_width.id,
            "type": "line",
            "source": "indoor_source",    
            "source-layer": "line",
            "minzoom": 17,
            "filter": ["all", startlevel, 
                      stairs_width.filter
                      ],   
            "paint": {
                
                'line-color': '#7e7f80',
                'line-width': {
                    'property': 'width', 
                    //'base': 20,      // 10, 25             
                     "stops": [[{zoom: 17, value: 1}, 2],[{zoom: 18, value: 1}, 8],[{zoom: 19, value: 1}, 15],[{zoom: 20, value: 1}, 30]]
                },                
                'line-dasharray': [0.05,0.1] // 0.2,0.4 first number line second number gap
                } // [1,8], [2,14], [3,22], [4,22],
        },   
          
        {
            "id": platform.id,
            "type": "fill",
            "source": "indoor_source",    
            "source-layer": "polygon",
            "filter": ["all", startlevel, 
                      platform.filter
                      ],   
            "paint": {
                "fill-color": "#C1C1C0",
                "fill-outline-color": "#7e7f80",
            }
        }, 
         {
              "id": fence.id,
            "minzoom": 19,
            "type": "line",
            "source": "indoor_source",    
            "source-layer": "line",
            "filter": ["all", startlevel, 
                      fence.filter
                      ],   
            "paint": {
                'line-color': 'black',
                'line-width': 1.5                    
                     
                }
        },   
        {
            "id": elevator.id,
            "type": "fill",
            "source": "indoor_source",    
            "source-layer": "polygon",
            "filter":
                     ["all", startlevel,
                     elevator.filter                  
                     ],     
            "paint": {
                "fill-color": "#C4C8CC",
                "fill-outline-color": "#7e7f80",
            }
        },     
        {
            "id": facilities.id,
            "type": "fill",
            "source": "indoor_source",    
            "source-layer": "polygon",
            "filter": ["all", startlevel, 
                      facilities.filter
                      ],   
            "paint": {
                "fill-color": "#066C9E", 
                "fill-outline-color": "#7e7f80",
            }
        },   
         {
            "id": service.id,
            //"minzoom": 17,
            "type": "fill",
            "source": "indoor_source", 

            "source-layer": "polygon",
            "filter": ["all", startlevel, 
                      service.filter
                      ],              
            "paint": {
                "fill-color": "#E7342B", 
                "fill-outline-color": "#7E7760",
            }
        },         
        {
            "id": footway.id,
            "minzoom": 18,
            "type": "line",
            "source": "indoor_source",    
            "source-layer": "line",
            "filter": ["all", startlevel, 
                      footway.filter
                      ],   
            "paint": {
                'line-color': '#ddd',
                'line-width': {                    
                     "stops": [[17,1], [18,2], [19,4], [20,6]]
                },                
                'line-dasharray': [1,1] // 0.2,0.4 first number line second number gap
            }
        },              
        {
            "id": door.id,
            "minzoom": 18,
            "type": "line",
            "source": "indoor_source",    
            "source-layer": "line",
            "filter": ["all", startlevel, 
                      door.filter
                      ],   
            "paint": {
                'line-color': '#EDEDED',
                'line-width': 1.2   
            }
        },  
        // ++++++++++++++ START LABEL ++++++++++++++++++++++   
         {
            "id": conveying_up.id,
            "minzoom": 19,
            "type": "symbol",
            "source": "indoor_source",    
            "source-layer": "line",            
            "layout": {
                  "icon-image": "steps-arrow-2.small",
                  "icon-size": 0.4,
                  "symbol-placement": "line", 
                  "symbol-spacing": 30,
            },
            "filter": ["all", startlevel, 
                      conveying_up.filter
                      ],   
           
        },
        
         {
           "id": conveying_down.id,
            "minzoom": 19,
            "type": "symbol",
            "source": "indoor_source",    
            "source-layer": "line",            
            "layout": {
                  "icon-image": "steps-arrow-2.small",
                  "icon-size": 0.4,
                  "symbol-placement": "line",                
                  "icon-rotate": 180,
                  "symbol-spacing": 30,
            },
            "filter": ["all", startlevel, 
                      conveying_down.filter
                      ],   
        },   
        {  
            "id": text_shop.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "label",
            "filter": ["all", startlevel, 
                      text_shop.filter
                      ],   
            "layout": {        
                  "text-offset": [0,0.5],
                  "text-field": "{name}",
                  "text-font": [ "Open Sans Regular", "Arial Unicode MS Regular"],
                  "text-max-width": 4,
                  "text-anchor": "top",
                  "text-size": 12                              
            }
        },
         {
            "id": bench.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "point",
            "filter": ["all", startlevel, 
                      bench.filter
                      ],   
            "layout": {
                  "icon-image": "wartebereich.small",
                  "icon-size": 0.4
            }
        },
        {
            "id": atm.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "point",
            "filter": ["all", startlevel, 
                      atm.filter
                      ],   
            "layout": {
                  "icon-image": "ec_atm.small",
                  "icon-size": 0.4
            }
        },
        {
            "id": ticket_validator.id,
            "minzoom": 19,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "point",
            "filter": ["all", startlevel, 
                      ticket_validator.filter
                      ],   
            "layout": {
                  "icon-image": "fahrkartenentwerter.small",
                  "icon-size": 0.4
            }
        },
        {  
            "id": label_bank.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "label",
            "filter": ["all", startlevel, 
                      label_bank.filter
                      ],   
            "layout": {                  
                  "icon-image": "bank-11" ,
                 // "icon-size": 0.3,
                  "text-offset": [0,0.5],
                  "text-field": "{name}",
                  "text-font": [ "Open Sans Regular", "Arial Unicode MS Regular"],
                  "text-max-width": 4,
                  "text-anchor": "top",
                  "text-size": 12                              
            }
        },
        {  
            "id": label_books.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "label",
            "filter": ["all", startlevel, 
                      label_books.filter
                      ],   
            "layout": {                  
                  "icon-image": "library-11" ,
                 // "icon-size": 0.3,
                  "text-offset": [0,0.5],
                  "text-field": "{name}",
                  "text-font": [ "Open Sans Regular", "Arial Unicode MS Regular"],
                  "text-max-width": 4,
                  "text-anchor": "top",
                  "text-size": 12                              
            }
        },
        {  
            "id": label_florist.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "label",
            "filter": ["all", startlevel, 
                      label_florist.filter
                      ],   
            "layout": {                  
                  "icon-image": "garden-11" ,
                 // "icon-size": 0.3,
                  "text-offset": [0,0.5],
                  "text-field": "{name}",
                  "text-font": [ "Open Sans Regular", "Arial Unicode MS Regular"],
                  "text-max-width": 4,
                  "text-anchor": "top",
                  "text-size": 12                              
            }
        },
        {  
            "id": label_bakery.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "label",
            "filter": ["all", startlevel, 
                      label_bakery.filter
                      ],   
            "layout": {                  
                  "icon-image": "bakery-11" ,
                 // "icon-size": 0.3,
                  "text-offset": [0,0.5],
                  "text-field": "{name}",
                  "text-font": [ "Open Sans Regular", "Arial Unicode MS Regular"],
                  "text-max-width": 4,
                  "text-anchor": "top",
                  "text-size": 12                              
            }
        },
        {  
            "id": label_restaurant.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "label",
            "filter": ["all", startlevel, 
                      label_restaurant.filter
                      ],   
            "layout": {                  
                  "icon-image": "restaurant-11" ,
                 // "icon-size": 0.3,
                  "text-offset": [0,0.5],
                  "text-field": "{name}",
                  "text-font": [ "Open Sans Regular", "Arial Unicode MS Regular"],
                  "text-max-width": 4,
                  "text-anchor": "top",
                  "text-size": 12                              
            }
        },
         {  
            "id": label_fast_food.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "label",
            "filter": ["all", startlevel, 
                      label_fast_food.filter
                      ],   
            "layout": {                  
                  "icon-image": "fast-food-11" ,
                 // "icon-size": 0.3,
                  "text-offset": [0,0.5],
                  "text-field": "{name}",
                  "text-font": [ "Open Sans Regular", "Arial Unicode MS Regular"],
                  "text-max-width": 4,
                  "text-anchor": "top",
                  "text-size": 12                              
            }
        },
         {  
            "id": label_cafe.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "label",
            "filter": ["all", startlevel, 
                      label_cafe.filter
                      ],   
            "layout": {                  
                  "icon-image": "cafe-11" ,
                 // "icon-size": 0.3,
                  "text-offset": [0,0.5],
                  "text-field": "{name}",
                  "text-font": [ "Open Sans Regular", "Arial Unicode MS Regular"],
                  "text-max-width": 4,
                  "text-anchor": "top",
                  "text-size": 12                              
            }
        },
         {  
            "id": label_car_rental.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "label",
            "filter": ["all", startlevel, 
                      label_car_rental.filter
                      ],   
            "layout": {                  
                  "icon-image": "car-11" ,
                 // "icon-size": 0.3,
                  "text-offset": [0,0.5],
                  "text-field": "{name}",
                  "text-font": [ "Open Sans Regular", "Arial Unicode MS Regular"],
                  "text-max-width": 4,
                  "text-anchor": "top",
                  "text-size": 12                              
            }
        },
        {  
            "id": label_pharmacy.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "label",
            "filter": ["all", startlevel, 
                      label_pharmacy.filter
                      ],   
            "layout": {                  
                  "icon-image": "hospital-11" ,
                 // "icon-size": 0.3,
                  "text-offset": [0,0.5],
                  "text-field": "{name}",
                  "text-font": [ "Open Sans Regular", "Arial Unicode MS Regular"],
                  "text-max-width": 4,
                  "text-anchor": "top",
                  "text-size": 12                              
            }
        },
        {  
            "id": label_clothes.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "label",
            "filter": ["all", startlevel, 
                      label_clothes.filter
                      ],   
            "layout": {                  
                  "icon-image": "clothing-store-11" ,
                 // "icon-size": 0.3,
                  "text-offset": [0,0.5],
                  "text-field": "{name}",
                  "text-font": [ "Open Sans Regular", "Arial Unicode MS Regular"],
                  "text-max-width": 4,
                  "text-anchor": "top",
                  "text-size": 12                              
            }
        },
                {  
            "id": label_information.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "label",
            "filter": ["all", startlevel, 
                      label_information.filter
                      ],   
            "layout": {                  
                  "icon-image": "information-11" ,
                 // "icon-size": 0.3,
                  "text-offset": [0,0.5],
                  "text-field": "{name}",
                  "text-font": [ "Open Sans Regular", "Arial Unicode MS Regular"],
                  "text-max-width": 4,
                  "text-anchor": "top",
                  "text-size": 12                              
            }
        },
        {  
            "id": label_luggage_locker.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "label",
            "filter": ["all", startlevel, 
                      label_luggage_locker.filter
                      ],   
            "layout": {                  
                  "icon-image": "schliessfach.small" ,
                  "icon-size": 0.4             
            }
        },  
        {
            "id": public_transport_tickets.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "point",
            "filter": ["all", startlevel, 
                      public_transport_tickets.filter
                      ],   
            "layout": {
                  "icon-image": "fahrkarten.small",
                  "icon-size": 0.4
            }
        },        
        {  
            "id": label_toilet.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "label",
            "filter": ["all", startlevel, 
                      label_toilet.filter
                      ],   
            "layout": {                  
                  "icon-image": "wc.small" ,
                  "icon-size": 0.4     
            }
        },
        {
            "id": "information_point",
            "minzoom": 17,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "point",
            "filter": ["all", startlevel, 
                      label_information.filter
                      ],   
            "layout": {
                  "icon-image": "information-11",
                  "icon-size": 0.4
            }
        },        
        {
            "id": elevatorpoint.id,
            "minzoom": 17,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "point",
            "filter": ["all", startlevel, 
                      elevatorpoint.filter
                      ],   
            "layout": {
                  "icon-image": "aufzug.small",
                  "icon-size": 0.4
            }
        },
         {  
            "id": service_symbol.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "label",
            "filter": ["all", startlevel, 
                      service_symbol.filter
                      ],   
            "layout": {                  
                  "icon-image": "i.small" ,
                  "icon-size": 0.4,
                  "text-offset": [0,1.2],
                  "text-field": "{name}",
                  "text-font": [ "Open Sans Regular", "Arial Unicode MS Regular"],
                  "text-max-width": 4,
                  "text-anchor": "top",
                  "text-size": 12                               
            }
        }, 
        {
            "id": local_ref1.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "point",
            "filter": ["all", startlevel, 
                      local_ref1.filter
                      ],   
            "layout": {
                  "icon-image": "gleis_1.small",
                  "icon-size": 0.4
            }
        },
        {
            "id": local_ref2.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "point",
            "filter": ["all", startlevel, 
                      local_ref2.filter
                      ],   
            "layout": {
                  "icon-image": "gleis_2.small",
                  "icon-size": 0.4
            }
        },
        {
            "id": local_ref3.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "point",
            "filter": ["all", startlevel, 
                      local_ref3.filter
                      ],   
            "layout": {
                  "icon-image": "gleis_3.small",
                  "icon-size": 0.4
            }
        },
        {
            "id": local_ref4.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "point",
            "filter": ["all", startlevel, 
                      local_ref4.filter
                      ],   
            "layout": {
                  "icon-image": "gleis_4.small",
                  "icon-size": 0.4
            },
        },    
        {
            "id": local_ref5.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "point",
            "filter": ["all", startlevel, 
                      local_ref5.filter
                      ],   
            "layout": {
                  "icon-image": "gleis_5.small",
                  "icon-size": 0.4
            },
        },    
        {
            "id": local_ref6.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "point",
            "filter": ["all", startlevel, 
                      local_ref6.filter
                      ],   
            "layout": {
                  "icon-image": "gleis_6.small",
                  "icon-size": 0.4
            },
        },    
        {
            "id": local_ref7.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "point",
            "filter": ["all", startlevel, 
                      local_ref7.filter
                      ],   
            "layout": {
                  "icon-image": "gleis_7.small",
                  "icon-size": 0.4
            }
        },
        {
            "id": local_ref8.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "point",
            "filter": ["all", startlevel, 
                      local_ref8.filter
                      ],   
            "layout": {
                  "icon-image": "gleis_8.small",
                  "icon-size": 0.4
            }
        },
        {
            "id": local_ref9.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "point",
            "filter": ["all", startlevel, 
                      local_ref9.filter
                      ],   
            "layout": {
                  "icon-image": "gleis_9.small",
                  "icon-size": 0.4
            }
        },
        {
            "id": local_ref10.id,
            "minzoom": 18,
            "type": "symbol",
            "source": "indoor_source", 

            "source-layer": "point",
            "filter": ["all", startlevel, 
                      local_ref10.filter
                      ],   
            "layout": {
                  "icon-image": "gleis_10.small",
                  "icon-size": 0.4
            }
        },
        { //------------------ S bahn label
            "interactive": true,
            "minzoom": 14,
            "maxzoom": 19,
            "layout": {
                "icon-image": "s.small",
                "text-offset": [
                    0,
                    0.6
                ],
                "text-field": "{name_en}",
                "text-font": [
                    "Open Sans Semibold",
                    "Arial Unicode MS Bold"
                ],
                "text-max-width": 8,
                "text-anchor": "top",
                "text-size": 15,
                "icon-size": 0.3
            },
            "filter": ["all",["==","$type","Point"],["==","maki","rail-light"],["==","network","de-s-bahn"]],
            "type": "symbol",
            "source": "mapbox",
            "id": "railstation_light_label",
            "paint": {
               
                "text-color": "black",                                
                "text-halo-width": 3,
                "text-halo-color": "rgba(255,255,255,0.75)",
                "text-halo-blur": 1
                
            },
            "source-layer": "rail_station_label"
        },
        {  //------------------ underground label
            "interactive": true,
            "minzoom": 14,
            "maxzoom": 19,
            "layout": {
                "icon-image": "u.small",
                "text-offset": [
                    0,
                    0.6
                ],
                "text-field": "{name_en}",
                "text-font": [
                    "Open Sans Semibold",
                    "Arial Unicode MS Bold"
                ],
                "text-max-width": 8,
                "text-anchor": "top",
                "text-size": 15,
                "icon-size": 0.3
            },
            "filter": ["all",["==","$type","Point"],["==","maki","rail-metro"],["==","network","de-u-bahn"]],
            "type": "symbol",
            "source": "mapbox",
            "id": "railstation_underground_label",
            "paint": {
                "text-color": "black",
                "text-halo-width": 3,
                "text-halo-color": "rgba(255,255,255,0.75)",
                "text-halo-blur": 1
            },
            "source-layer": "rail_station_label"
        },        
        { //------------------ bahn label
            "interactive": true,
            "minzoom": 12,
            "maxzoom": 19,
            "layout": {
                "icon-image": "rail-light-15", // db_logo.small
                "text-offset": [
                    0,
                    0.6
                ],
                "text-field": "{name_en}",
                "text-font": [
                    "Open Sans Semibold",
                    "Arial Unicode MS Bold"
                ],
                "text-max-width": 8,
                "text-anchor": "top",
                "text-size": 15,
                "icon-size": 1
            },
            "filter": ["all",["==","$type","Point"],["==","maki","rail"],["==","network","rail"]],
            "type": "symbol",
            "source": "mapbox",
            "id": "railstation_label",
            "paint": {
                "text-color": "black",
                "text-halo-width": 3,
                "text-halo-color": "rgba(255,255,255,0.75)",
                "text-halo-blur": 1
            },
            "source-layer": "rail_station_label"
        }       
    ]
}
