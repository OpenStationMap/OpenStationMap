/*
<link href="./gecoder/gecoder.css" rel="stylesheet">
<script src='./gecoder/gecoder.js'></script>

<script>
Starte Script mit: levelbar.create();
</script>
*/

        //---------TIME-------- 
        //var end = new Date().getTime();
        //var time = end - start;
        //console.log('Execution time find all level old: ' + time);
        //------------------------------  


var levelbar = (function () {

	var create = function () {
		
        // create container for level butttons
        var levelcont = document.createElement("div");     
        levelcont.setAttribute("id", "levelcont");       
        levelcont.classList.add("panel");
        document.getElementById("map").appendChild(levelcont);

		var rawlevel, startlevel;    	
    	var cleanlevel = []; 
    	
        //returns all levels which are in the layer
        rawlevel = findalllevel(map); 

    	// seperates levels and clean up like ";1" or "EG" or "1.3"  
    	cleanlevel = getindividuallevel(rawlevel);        	
    	
    	//bring levels in the right order
    	cleanlevel.sort(function(a, b){return b-a});  
        
    	//create new div elements which show the levels
    	var laenge = cleanlevel.length;
        var button, level;    
    	for(var i = 0 ; i < laenge; i++){     
            level = cleanlevel[i];            
            button = document.createElement("button");
            button.classList.add("levelbuttons");
            button.setAttribute("id", "level" + cleanlevel[i]);
            button.innerHTML = cleanlevel[i];
            button.addEventListener("click", function (level){
                // wrapper function to send the right level to listener
                return function() {levelclick(level, rawlevel);
                }
            }(level));
            document.getElementById("levelcont").appendChild(button);
    	}   
        
        // add eventlistener for mapchange so levelbar gets updated when move
        map.on('moveend',function(){update();});
        
        // set elements for first visualisation, also check mapstyle.js startlevel for start
        startlevel = "0";
        levelclick(startlevel, rawlevel);
	};    

    var update = function(){ 

        

        var rawlevel, cleanlevel, selectedLevelbutton;

        // if no old level is available take zero 
        var selectedLevel = "0";
        
        //returns all levels which are in the layer
        rawlevel = findalllevel(map);

        
        // seperates levels and clean up like ";1" or "EG" or "1.3"  
        cleanlevel = getindividuallevel(rawlevel);                  
        
        //bring levels in the right order
        cleanlevel.sort(function(a, b){return b-a});       

        //save old selected level to add later to levelbar again
        selectedLevelbutton = document.getElementsByClassName("levelbuttonsselected");
        if (selectedLevelbutton.length === 1){
            selectedLevel = selectedLevelbutton[0].innerHTML
        }     

        // delete old level buttons and hide box shadow         
        var conti = document.getElementById("levelcont");
        while (conti.firstChild) {
            conti.removeChild(conti.firstChild);
        }
        conti.setAttribute("style", "box-shadow:0px 0px 0px 0px");

        //in case no new levels are available go back
        if (rawlevel.length === 0){            
            return;
        }

        //create new div elements which show the levels
        var laenge = cleanlevel.length;
        var button, level;    
        for(var i = 0 ; i < laenge; i++){     
            level = cleanlevel[i];            
            button = document.createElement("button");
            button.classList.add("levelbuttons");
            button.setAttribute("id", "level" + cleanlevel[i]);
            button.innerHTML = cleanlevel[i];
            button.addEventListener("click", function (level){
                // wrapper function to send the right level to listener
                return function() {levelclick(level, rawlevel);
                }
            }(level));
            document.getElementById("levelcont").appendChild(button);
        }
        conti.setAttribute("style", "box-shadow:0px 0px 0px 2px rgba(0,0,0,0.1)");
        
        // take the old selected level and update map
        levelclick(selectedLevel, rawlevel);  
       
    };

    var levelclick = function (level, rawlevel){
        var button, oldbutton;
        // set all level for graphics
        selectLevel(level,rawlevel);
        // remove Styling from old Level
        oldbutton = document.getElementsByClassName("levelbuttonsselected");        
        // if statement just for first start where no class "levelbuttonsselected" is set yet
        if(oldbutton.length === 1){            
            oldbutton[0].classList.add("levelbuttons");
            oldbutton[0].classList.remove("levelbuttonsselected");
        } 
        // add Styling to new Level Button
        button = document.getElementById("level"+level);
        // check if button is avaiable in case user starts in empty indoor region
        if(button !== null){
            button.classList.add("levelbuttonsselected");
            button.classList.remove("levelbuttons");
        }
        
    };

    var findalllevel = function (map){ 
        var _map = map;   
        var sourcelayer = ["point", "line", "polygon"];       
        var rawlevel = [], features;    
        var i = 0;
        //loop through allindoor layer to find all level
        for(i; i < sourcelayer.length; i++){
            features = _map.querySourceFeatures('indoor_source', {sourceLayer: sourcelayer[i] ,filter: ['has', 'level']});  
            
            var j = 0;
            // in each indoor layer check each element for the level tag, if the feature from feature.prperties doesnt exist in rawlevel put it there
            for(j; j < features.length; j++){
                if(rawlevel.indexOf(features[j].properties.level) === -1){
                    rawlevel.push(features[j].properties.level);
                }
            }           
        }  
        return rawlevel;
    };

    var getindividuallevel = function(rawlevel){

        //take apart strings like "1;2"
        var i = 0, workingarray = [], singleLevel;

        for(i; i < rawlevel.length; i++){
            singleLevel = rawlevel[i].trim();
            if(singleLevel.includes(";")){            
                var doubleLevel= [], j = 0; 
                doubleLevel = singleLevel.split(";");
                var end = doubleLevel.length;
                for(j; j < end; j++){
                    if(workingarray.indexOf(doubleLevel[j]) === -1){ 
                    workingarray.push(doubleLevel[j]); 
                    }
                }  
            } else {                
                if(workingarray.indexOf(singleLevel) === -1){
                    workingarray.push(singleLevel); 
                }

            }
        }   

        // this part cleans up the single level strings like "1" or "1;2" or ";1"       
        var singleLevel2;
        var cleanlevel = [];
        var i = 0, end = workingarray.length;
       
        for(i; i < end; i++){
            singleLevel2 = workingarray[i].trim();
            // check for integer number and if already in claenlevel array
            // TODO hier kommmen immer noch level wie "03" durch. liegt aber nicht an der 2 :)            
            if(/^\-?[0-9]\d{0,2}$/.test(singleLevel2) && cleanlevel.indexOf(singleLevel2) === -1){             
                cleanlevel.push(singleLevel2);            
            }    
        }
        return cleanlevel;
    };
    

    var selectLevel = function (level, rawlevel){
        
        var j = 0, end = rawlevel.length, ele, levelsarray = [], info2;           
        var info = level;                 

            //create helper strings for Regular Expresions  
        if(info > 0){
                //console.log("info größer als null und zwar " + info);
            info2 = eval("info - 1"); //info-1;
                //console.log(info2);
        } else if(info < -1){
                //console.log("info kleiner als minus 1 und zwar: " + info);
            info2 = parseInt(info)+1;//eval("(info) - 1");
                //console.log(info2);
        } else if (info == 0){
                //console.log("info gleich null und zwar " + info);
            info2 = "-"+info;
                
        }else if (info == -1){
                //console.log("info gleich -1 und zwar " + info);
            info2 = "-0";
        }        
       
        var positiveexact = new RegExp('[;\s+]'+info+'[\s;]');
        var endexact = new RegExp('[;]' + info + '$');
        var begexact = new RegExp('^' + info + '[;]');
        var positiveval = new RegExp('[;+\\s]'+info+'\\.[0-5;]');
        var regex1 = new RegExp('^'+info+'\\.[0-5;]');
        var regex2 = new RegExp('[;+\\s]'+info+'\\.[0-5;]$');
        var regex3 = new RegExp('[;+\\s]'+info2+'\\.[5-9;]');
        var regex4 = new RegExp('^'+info2+'\\.[5-9;]');
        var regex5 = new RegExp('[;+\\s]'+info2+'\\.[5-9;]$');
        var zerocase1 = new RegExp('[;+\\s]'+info2+'\\.[0-5;]');
        var zerocase2 = new RegExp('^'+info2+'\\.[0-5;]');
        var zerocase3 = new RegExp('[;+\\s]'+info2+'\\.[0-5;]$');           
            
        for(j ; j < end; j++){
            ele = rawlevel[j];
            
            if(info === ele || positiveexact.test(ele) || endexact.test(ele) || begexact.test(ele)){
                // TODO: exakter wert muss ins array. aber nicht hier wegen doppelter arbeit. einfach von info nehmen und einfügen
                //console.log("Level: " + info + " genau gleich ... irgendwo");
                levelsarray.push(ele); 
                continue;
            }else if (positiveval.test(ele) || regex1.test(ele) || regex2.test(ele)){
                //console.log("level: "+info+" double pression höher als wert");
                levelsarray.push(ele); 
                continue
            }else if (info !== 0 && (regex3.test(ele) || regex4.test(ele) || regex5.test(ele))){
                //console.log("level: "+info+" einen weniger und nur .5 bis .9"); 
                levelsarray.push(ele);
                continue;                    
            }else if (info === 0 && (zerocase1.test(ele) || zerocase2.test(ele) || zerocase3.test(ele))){
                levelsarray.push(ele);
                //console.log("Special case für Null;  level: "+info+" müsste -0.5 bis -0.1 abdecken"); 
                continue;                    
            }
        }  
    
        var k = 0;
        var indoorLayer = indoorObjects.length;        
        for(k ; k < indoorLayer; k++){       
            var arra = [];
            arra.push(indoorObjects[k].filter);      
            var filtered = ["all",["in", "level", ].concat(levelsarray)].concat(arra);
            map.setFilter(indoorObjects[k].id, filtered);
        }    
    }

		return {
    		create: create,
            update: update
  		}; 

}());	