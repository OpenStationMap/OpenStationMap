// depends on jquery and a map container called 'map'

//eventlistener for searchbox, see geocoder.js
$("#searchtext").keyup(function(event){    
    if(event.which == 13){
        requestLocations();
    }    
});
$("#searchsymbol").click(function(){
    requestLocations();
});



function requestLocations(){

	function clearResults(){
		// hide list after click on one element
		$("#resultlist").css("display", "none");
		// get rid of all event listener on the search results
		$('.suggestions').children().off('click');
		// remove the search results from the list
		$('.suggestions').remove();		
	};
	// make clearResults ready to call it from outside the scope
	requestLocations.clearResults = clearResults;

	//get text from search field and trim
	var querystring = $("#searchtext").val().trim();	
	
	// only take strings which are longer or equal 3
	if(querystring.length >= 3){
		
		// check string if it contains whitespaces
		if(querystring.indexOf(" ") != -1){
			// replace whitespaces with "+" , necessary for nominatim query
			var temporal = querystring.replace(/ /g, "+");
			querystring = temporal;
		}
		
		// send query to nominatim and handle the answer
		$.ajax({
			url: "http://nominatim.openstreetmap.org/?format=json&addressdetails=1&q=" 
			+ querystring 
			+ "+[station]&format=json&limit=5"
		}).done(function(data){
			var response = data;			
			//remove old results
			$('.suggestions').remove();
			//activate list to see the results in browser
			$("#resultlist").css("display", "block");
			// go through response and display them
			if(response.length > 0){				
				
				var singleResponse;
				//add each element from response to list
				for(var i = 0 ; i < response.length ; i++){ // response.length
					singleResponse = response[i];										
					// add content to list and click listener					
					$('<li class="suggestions" id="suggestionNumber' + i + '">' 
						+ "<strong>" + singleResponse.address.station + "</strong>" 
						+ "<br>"
						+ singleResponse.address.state
						+ ", " 
						+ singleResponse.address.country 
						+'</li>').appendTo("#resultlist")
								// add event listener to the results from query, a click leads to the area of interest
								 .click({gid: singleResponse},function(e){																	
									map.setCenter([e.data.gid.lon , e.data.gid.lat]);
									map.setZoom(17);
									clearResults();									
							 });

				}
			}
			else{
				//no result came back from Nominatim, shows just a nothing found message
				$('<li class="suggestions" id="suggestionNumber0">' 
						+ "<strong> Nothing was found </strong>" 						
						+'</li>').appendTo("#resultlist");

			}			
		});
	} 
	else{
		// sets list container back to unvisible in case user didnt input at least three character
		clearResults();		
		return;
	}
	
}


(function (document,map) {

	function createsearchbox(){

		var searchbox = document.createElement("form");
		searchbox.setAttribute("id", "searcher2");
		searchbox.style.top = "70px";
		searchbox.style.left = "10px";	
		searchbox.classList.add("panel");
		document.getElementById("map").appendChild(searchbox);

		var searchsymbol = document.createElement("div");
		searchsymbol.setAttribute("id", "searchsymbol2");
		searchsymbol.style.float = "left";
		searchsymbol.style.height = "40px";
		searchsymbol.style.width = "40px";
		searchsymbol.style.cursor = "pointer";
		document.getElementById("searcher2").appendChild(searchsymbol);

		var searchsymbolpic = document.createElement("img");
		searchsymbolpic.src = "http://85.214.126.95/vectormap/0.1.0/javascript/geocoder/lupe.svg";
		searchsymbolpic.style.display = "block";
		searchsymbolpic.style.marginLeft = "auto";
		searchsymbolpic.style.marginRight = "auto";
		searchsymbolpic.style.marginTop = "8px";
		document.getElementById("searchsymbol2").appendChild(searchsymbolpic);

		var searchfield = document.createElement("input");
		searchfield.setAttribute("id", "searchfield2");
		searchfield.style.marginTop = "8px";
		searchfield.style.width = "190px";
		searchfield.style.border = "none";
		searchfield.style.outline = "none";
		document.getElementById("searcher2").appendChild(searchfield);

		var resultlist = document.createElement("ul");
		resultlist.setAttribute("id", "resultlist2");
		resultlist.style.display = "none";
		resultlist.style.width = "230px";
		resultlist.style.listStyleType = "none";
		resultlist.style.padding = "0px";
		resultlist.style.marginBottom = "0px";
		document.getElementById("searcher2").appendChild(resultlist);

	}

	//createsearchbox();

	function clearResults(){
		// hide list after click on one element
		$("#resultlist2").css("display", "none");
		// get rid of all event listener on the search results
		$('.suggestions2').children().off('click');
		// remove the search results from the list
		$('.suggestions2').remove();		
	};

	
	

 

}(document,map));

