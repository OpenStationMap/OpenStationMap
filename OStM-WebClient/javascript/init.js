map.on('load', function () {  
    levelbar.create();
    // add searchbox attached to map
    geocoder.create();
});

map.on('click', function (e) {
   
    // add popup with details of the map
    popupDetails(e);    
});

function popupDetails(e){
    // add popup with map details on click
    var featuresRendered = map.queryRenderedFeatures(e.point); 
    var properties = [];
    var nummer = 1;
    for(var i = 0 ; i < featuresRendered.length; i++){  
        if(featuresRendered[i].properties.hasOwnProperty("level")){
           
            properties.push("Object " + nummer + ": " + JSON.stringify(featuresRendered[i].properties) + "<br>");
        }  
        nummer++;
    }
    if (!properties.length) {
        return;
    } 
    var width = document.getElementById("map").offsetWidth-100;    
    var popup = new mapboxgl.Popup()
        .setLngLat(e.lngLat)
        .setHTML("<div style='font-size:12pt; max-width: "+ width +"px; overflow: scroll;'>" + properties + "</div>")
        .addTo(map);
}

/*
map.on('zoomend', function () {   
    console.log(map.getZoom());     
});
*/
