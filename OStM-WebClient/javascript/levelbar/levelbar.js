function getindividuallevel(rawlevel){
    // this function cleans up the single level strings like "1" or "1;2" or ";1"       
    var singleLevel;
    var cleanlevel = [];

    var i = 0, end = rawlevel.length;
       
    for(i; i < end; i++){
        singleLevel = rawlevel[i].trim();
        // check for integer number and if already in claenlevel array
        // TODO hier kommmen immer noch level wie "03" durch. liegt aber nicht an der 2 :)
        if(/^\-?[0-9]\d{0,2}$/.test(singleLevel) && $.inArray(singleLevel, cleanlevel) === -1){             
            cleanlevel.push(singleLevel);            
        }    
    }    
    return cleanlevel;  
}

// check for ";" in levelstring. If there split array. if okay add to cleanlevel
function getsublevel(rawlevel){
    var i = 0, workingarray = [], singleLevel;
       
    for(i; i < rawlevel.length; i++){
        singleLevel = rawlevel[i].trim();
        if(singleLevel.includes(";")){
            
            var doubleLevel= [], j = 0; 
            doubleLevel = singleLevel.split(";");
            var ende = doubleLevel.length;            
            
            for(j; j < ende; j++){ 
                
                if($.inArray(doubleLevel[j], workingarray) === -1){ 
                workingarray.push(doubleLevel[j]); 
                }
            }
        } else {
            if($.inArray(singleLevel, workingarray) === -1){ 
                workingarray.push(singleLevel); 
            }
        }
    } 
    return workingarray;
}

function selectLevel(level, rawlevel){

    //var start = new Date().getTime();
    var j = 0, end = rawlevel.length, ele, levelsarray = [], info2;           
    var info = level;                 

            //create helper strings for Regular Expresions  
    if(info > 0){
                //console.log("info größer als null und zwar " + info);
        info2 = eval("info - 1"); //info-1;
                //console.log(info2);
    } else if(info < -1){
                //console.log("info kleiner als minus 1 und zwar: " + info);
                info2 = parseInt(info)+1;//eval("(info) - 1");
                //console.log(info2);
    } else if (info == 0){
                //console.log("info gleich null und zwar " + info);
        info2 = "-"+info;
                
    }else if (info == -1){
                //console.log("info gleich -1 und zwar " + info);
        info2 = "-0";
    }        
       
    var positiveexact = new RegExp('[;\s+]'+info+'[\s;]');
    var endexact = new RegExp('[;]' + info + '$');
    var begexact = new RegExp('^' + info + '[;]');
    var positiveval = new RegExp('[;+\\s]'+info+'\\.[0-5;]');
    var regex1 = new RegExp('^'+info+'\\.[0-5;]');
    var regex2 = new RegExp('[;+\\s]'+info+'\\.[0-5;]$');
    var regex3 = new RegExp('[;+\\s]'+info2+'\\.[5-9;]');
    var regex4 = new RegExp('^'+info2+'\\.[5-9;]');
    var regex5 = new RegExp('[;+\\s]'+info2+'\\.[5-9;]$');
    var zerocase1 = new RegExp('[;+\\s]'+info2+'\\.[0-5;]');
    var zerocase2 = new RegExp('^'+info2+'\\.[0-5;]');
    var zerocase3 = new RegExp('[;+\\s]'+info2+'\\.[0-5;]$');           
            
    for(j ; j < end; j++){
        ele = rawlevel[j];
        //console.log("wert auf ele: " + ele);                
                
        if(info === ele || positiveexact.test(ele) || endexact.test(ele) || begexact.test(ele)){
            // TODO: exakter wert muss ins array. aber nicht hier wegen doppelter arbeit. einfach von info nehmen und einfügen
            //console.log("Level: " + info + " genau gleich ... irgendwo");
            levelsarray.push(ele); 
            continue;
        } else if (positiveval.test(ele) || regex1.test(ele) || regex2.test(ele)){

            //console.log("level: "+info+" double pression höher als wert");
            levelsarray.push(ele); 
            continue

        }  else if (info !== 0 && (regex3.test(ele) || regex4.test(ele) || regex5.test(ele))){

            //console.log("level: "+info+" einen weniger und nur .5 bis .9"); 
            levelsarray.push(ele);
            continue;
                    
        }  else if (info === 0 && (zerocase1.test(ele) || zerocase2.test(ele) || zerocase3.test(ele))){
            levelsarray.push(ele);
            //console.log("Special case für Null;  level: "+info+" müsste -0.5 bis -0.1 abdecken"); 
            continue;                    
        }

    }  
    
    var k = 0;
    var indoorLayer = indoorObjects.length;
    
    for(k ; k < indoorLayer; k++){
       
        var arra = [];
        arra.push(indoorObjects[k].filter);      
        var filtered = ["all",["in", "level", ].concat(levelsarray)].concat(arra);
        map.setFilter(indoorObjects[k].id, filtered);
    }

    //---------TIME-------- 
    //var end = new Date().getTime();
    //var time = end - start;
    //console.log('Execution time find set all FILTER: ' + time);
    //------------------------------
    
}

function findalllevel(){
    
    var start = new Date().getTime();
    var rawlevel = [];

    var features = map.querySourceFeatures('indoor_source', {sourceLayer: 'polygon',filter: ['has', 'level']});     
    // query through features to get new level    
    for (let i of features) {
        if($.inArray(i.properties.level, rawlevel) === -1){
                rawlevel.push(i.properties.level);                                         
        }
    }
    var features2 = map.querySourceFeatures('indoor_source', {sourceLayer: 'line',filter: ['has', 'level']}); 
    for (let i of features2) {
        if($.inArray(i.properties.level, rawlevel) === -1){
            rawlevel.push(i.properties.level);                                         
        } 
    }
    var features3 = map.querySourceFeatures('indoor_source', {sourceLayer: 'point',filter: ['has', 'level']}); 
    for (let i of features3) {  
        if($.inArray(i.properties.level, rawlevel) === -1){
            rawlevel.push(i.properties.level);                                         
        } 
    }
    //---------TIME-------- 
    //var end = new Date().getTime();
    //var time = end - start;
    //console.log('Execution time find all level: ' + time);
    //------------------------------
    return rawlevel;
}

function makeLevelbar(){

    var rawlevel = findalllevel(); 
    var singleElementsArray = [];
    var cleanlevel = []; 
    var selectedLevel;
    
    //seperate levels... like "0;1" 
    singleElementsArray = getsublevel(rawlevel);    
    // clean up levels like ";1" or "EG" or "1.3"  
    cleanlevel = getindividuallevel(singleElementsArray); 
    // delete old level container and go back if array is empty therefore no levels availible       
    if(!cleanlevel.length){
        // check if some old level container are availible
        if($('.leveli').length){            
            $('.leveli').remove();
        }        
        return;
    }
    //save old selected level to add later to levelbar again
    if($('.leveliSelected')){
        selectedLevel = $('.leveliSelected');
    }  
    //bring levels in the right order
    cleanlevel.sort(function(a, b){return b-a}) 
    
    // remove the old level div elements
    $('.leveli').children().off('click');
    $('.leveli').remove();
    //create new div elements which show the levels
    var laenge = cleanlevel.length;
    
    for(var i = 0 ; i < laenge; i++){           
        $('<button class="leveli" id="level'+ cleanlevel[i] +'">'+cleanlevel[i]+'</button>').appendTo("#menu");
        $('#level'+cleanlevel[i]+'').click( function(){
            var level = $(this).html(); 
            level.trim            
            selectLevel(level, rawlevel);            
            $('.leveli').removeClass('leveliSelected');            
            $(this).addClass('leveliSelected');
        });      
    } 
    // keep old selected level alive after re-new the levelbar, if none exist set level 0
    if(typeof selectedLevel !== 'undefined' && selectedLevel.length){        
        $('#' + selectedLevel[0].id).trigger('click'); 
    } else{
        $('#level0').trigger('click');
    } 
}
